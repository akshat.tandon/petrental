package com.test.licenseservice.resource;

import com.test.licenseservice.models.Licenese;
import com.test.licenseservice.models.LicenseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class LicenseResource {

    private final Logger log = LoggerFactory.getLogger(LicenseResource.class);

    @PostMapping("/license")
    public LicenseStatus validateLicense(@RequestBody Licenese license) {
        log.info("REST request to validate license : {}", license.getLicensename());
        return new LicenseStatus(license.getLicensename(), true);
    }

    @RequestMapping ("/license")
    public LicenseStatus getLicense() {
        log.info("REST request to get license : {}", "abc");
        return new LicenseStatus("abc", true);
    }
}
