package com.test.licenseservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class LicenseserviceApplication {

	public static void main(String[] args) {
        SpringApplication.run(LicenseserviceApplication.class, args);
	}


}
