# petrental

## Flow

Flow
![Flow](petrental.png)

## Prerequisites

1. [Node.js][]: We use Node to run a development web server and build the frontend.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

2. [yarn][]: we need to have yarn intalled locally.

3. [Java][]: For Running backend services you need to have java8 intalled locally.


## Running
Project consists of 3 subservices
a) Discovery service 
b) License service
c) PetRental app

###  a) Discovery service 
It is a service discovery third party server for finding service run using Eureka server
To run go inside discoveryserver folder and run for Windows OS

```
gradlew.bat bootRun
```
Or for other OS , Give permission to execute chmod 755 gradlew
```
./gradlew bootRun
```

###  b) License service
It is a dummy third party License Validator server for validating licesenses
To run go inside licenseservice folder and run for Windows OS

```
gradlew.bat bootRun
```
Or for other OS , Give permission to execute chmod 755 gradlew
```
./gradlew bootRun
```


###  C) PetRental App
Core Petrental service to booking cars on rent.
It has 2 parts 

1) Backend

To run go inside petrentalapp folder and run for Windows OS

```
gradlew.bat
```
Or for other OS , Give permission to execute chmod 755 gradlew
```
./gradlew
```
2) Front End
To run go inside petrentalapp folder and run yarn to install any depedencies 
```
yarn
```
and run yarn start to start frontend
```
yarn start
```
## App Details
### Service Discovery 
http://localhost:8761
### Licensing Service
http://localhost:8011
### PetRental Backend 
http://localhost:8080
### PetRental Application 
http://localhost:9000

Usernames: 
admin/admin
user/user


