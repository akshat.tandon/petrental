import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { PetrentalTestModule } from '../../../test.module';
import { BookingDetailComponent } from 'app/entities/booking/booking-detail.component';
import { Booking } from 'app/shared/model/booking.model';

describe('Component Tests', () => {
  describe('Booking Management Detail Component', () => {
    let comp: BookingDetailComponent;
    let fixture: ComponentFixture<BookingDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ booking: new Booking(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PetrentalTestModule],
        declarations: [BookingDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookingDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookingDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load booking on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.booking).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
