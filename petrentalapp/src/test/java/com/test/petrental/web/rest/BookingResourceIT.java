package com.test.petrental.web.rest;

import com.test.petrental.PetrentalApp;
import com.test.petrental.domain.Booking;
import com.test.petrental.repository.BookingRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BookingResource} REST controller.
 */
@SpringBootTest(classes = PetrentalApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class BookingResourceIT {

    private static final Instant DEFAULT_BOOKINGTIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BOOKINGTIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_BOOKING_STARTTIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BOOKING_STARTTIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_BOOKING_ENDTIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BOOKING_ENDTIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_PICKUP_POINT = "AAAAAAAAAA";
    private static final String UPDATED_PICKUP_POINT = "BBBBBBBBBB";

    private static final String DEFAULT_DROP_POINT = "AAAAAAAAAA";
    private static final String UPDATED_DROP_POINT = "BBBBBBBBBB";

    private static final byte[] DEFAULT_LICENSE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_LICENSE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_LICENSE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_LICENSE_CONTENT_TYPE = "image/png";

    private static final Boolean DEFAULT_LICENSE_VALID = false;
    private static final Boolean UPDATED_LICENSE_VALID = true;

    @Autowired
    private BookingRepository bookingRepository;

    @Mock
    private BookingRepository bookingRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBookingMockMvc;

    private Booking booking;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Booking createEntity(EntityManager em) {
        Booking booking = new Booking()
            .bookingtime(DEFAULT_BOOKINGTIME)
            .bookingStarttime(DEFAULT_BOOKING_STARTTIME)
            .bookingEndtime(DEFAULT_BOOKING_ENDTIME)
            .pickupPoint(DEFAULT_PICKUP_POINT)
            .dropPoint(DEFAULT_DROP_POINT)
            .license(DEFAULT_LICENSE)
            .licenseContentType(DEFAULT_LICENSE_CONTENT_TYPE)
            .licenseValid(DEFAULT_LICENSE_VALID);
        return booking;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Booking createUpdatedEntity(EntityManager em) {
        Booking booking = new Booking()
            .bookingtime(UPDATED_BOOKINGTIME)
            .bookingStarttime(UPDATED_BOOKING_STARTTIME)
            .bookingEndtime(UPDATED_BOOKING_ENDTIME)
            .pickupPoint(UPDATED_PICKUP_POINT)
            .dropPoint(UPDATED_DROP_POINT)
            .license(UPDATED_LICENSE)
            .licenseContentType(UPDATED_LICENSE_CONTENT_TYPE)
            .licenseValid(UPDATED_LICENSE_VALID);
        return booking;
    }

    @BeforeEach
    public void initTest() {
        booking = createEntity(em);
    }

    @Test
    @Transactional
    public void createBooking() throws Exception {
        int databaseSizeBeforeCreate = bookingRepository.findAll().size();
        // Create the Booking
        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isCreated());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeCreate + 1);
        Booking testBooking = bookingList.get(bookingList.size() - 1);
        assertThat(testBooking.getBookingtime()).isEqualTo(DEFAULT_BOOKINGTIME);
        assertThat(testBooking.getBookingStarttime()).isEqualTo(DEFAULT_BOOKING_STARTTIME);
        assertThat(testBooking.getBookingEndtime()).isEqualTo(DEFAULT_BOOKING_ENDTIME);
        assertThat(testBooking.getPickupPoint()).isEqualTo(DEFAULT_PICKUP_POINT);
        assertThat(testBooking.getDropPoint()).isEqualTo(DEFAULT_DROP_POINT);
        assertThat(testBooking.getLicense()).isEqualTo(DEFAULT_LICENSE);
        assertThat(testBooking.getLicenseContentType()).isEqualTo(DEFAULT_LICENSE_CONTENT_TYPE);
        assertThat(testBooking.isLicenseValid()).isEqualTo(DEFAULT_LICENSE_VALID);
    }

    @Test
    @Transactional
    public void createBookingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bookingRepository.findAll().size();

        // Create the Booking with an existing ID
        booking.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkBookingtimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setBookingtime(null);

        // Create the Booking, which fails.


        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBookingStarttimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setBookingStarttime(null);

        // Create the Booking, which fails.


        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBookingEndtimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setBookingEndtime(null);

        // Create the Booking, which fails.


        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPickupPointIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setPickupPoint(null);

        // Create the Booking, which fails.


        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDropPointIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setDropPoint(null);

        // Create the Booking, which fails.


        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBookings() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);

        // Get all the bookingList
        restBookingMockMvc.perform(get("/api/bookings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(booking.getId().intValue())))
            .andExpect(jsonPath("$.[*].bookingtime").value(hasItem(DEFAULT_BOOKINGTIME.toString())))
            .andExpect(jsonPath("$.[*].bookingStarttime").value(hasItem(DEFAULT_BOOKING_STARTTIME.toString())))
            .andExpect(jsonPath("$.[*].bookingEndtime").value(hasItem(DEFAULT_BOOKING_ENDTIME.toString())))
            .andExpect(jsonPath("$.[*].pickupPoint").value(hasItem(DEFAULT_PICKUP_POINT)))
            .andExpect(jsonPath("$.[*].dropPoint").value(hasItem(DEFAULT_DROP_POINT)))
            .andExpect(jsonPath("$.[*].licenseContentType").value(hasItem(DEFAULT_LICENSE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].license").value(hasItem(Base64Utils.encodeToString(DEFAULT_LICENSE))))
            .andExpect(jsonPath("$.[*].licenseValid").value(hasItem(DEFAULT_LICENSE_VALID.booleanValue())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllBookingsWithEagerRelationshipsIsEnabled() throws Exception {
        when(bookingRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restBookingMockMvc.perform(get("/api/bookings?eagerload=true"))
            .andExpect(status().isOk());

        verify(bookingRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllBookingsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(bookingRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restBookingMockMvc.perform(get("/api/bookings?eagerload=true"))
            .andExpect(status().isOk());

        verify(bookingRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getBooking() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);

        // Get the booking
        restBookingMockMvc.perform(get("/api/bookings/{id}", booking.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(booking.getId().intValue()))
            .andExpect(jsonPath("$.bookingtime").value(DEFAULT_BOOKINGTIME.toString()))
            .andExpect(jsonPath("$.bookingStarttime").value(DEFAULT_BOOKING_STARTTIME.toString()))
            .andExpect(jsonPath("$.bookingEndtime").value(DEFAULT_BOOKING_ENDTIME.toString()))
            .andExpect(jsonPath("$.pickupPoint").value(DEFAULT_PICKUP_POINT))
            .andExpect(jsonPath("$.dropPoint").value(DEFAULT_DROP_POINT))
            .andExpect(jsonPath("$.licenseContentType").value(DEFAULT_LICENSE_CONTENT_TYPE))
            .andExpect(jsonPath("$.license").value(Base64Utils.encodeToString(DEFAULT_LICENSE)))
            .andExpect(jsonPath("$.licenseValid").value(DEFAULT_LICENSE_VALID.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingBooking() throws Exception {
        // Get the booking
        restBookingMockMvc.perform(get("/api/bookings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBooking() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);

        int databaseSizeBeforeUpdate = bookingRepository.findAll().size();

        // Update the booking
        Booking updatedBooking = bookingRepository.findById(booking.getId()).get();
        // Disconnect from session so that the updates on updatedBooking are not directly saved in db
        em.detach(updatedBooking);
        updatedBooking
            .bookingtime(UPDATED_BOOKINGTIME)
            .bookingStarttime(UPDATED_BOOKING_STARTTIME)
            .bookingEndtime(UPDATED_BOOKING_ENDTIME)
            .pickupPoint(UPDATED_PICKUP_POINT)
            .dropPoint(UPDATED_DROP_POINT)
            .license(UPDATED_LICENSE)
            .licenseContentType(UPDATED_LICENSE_CONTENT_TYPE)
            .licenseValid(UPDATED_LICENSE_VALID);

        restBookingMockMvc.perform(put("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBooking)))
            .andExpect(status().isOk());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeUpdate);
        Booking testBooking = bookingList.get(bookingList.size() - 1);
        assertThat(testBooking.getBookingtime()).isEqualTo(UPDATED_BOOKINGTIME);
        assertThat(testBooking.getBookingStarttime()).isEqualTo(UPDATED_BOOKING_STARTTIME);
        assertThat(testBooking.getBookingEndtime()).isEqualTo(UPDATED_BOOKING_ENDTIME);
        assertThat(testBooking.getPickupPoint()).isEqualTo(UPDATED_PICKUP_POINT);
        assertThat(testBooking.getDropPoint()).isEqualTo(UPDATED_DROP_POINT);
        assertThat(testBooking.getLicense()).isEqualTo(UPDATED_LICENSE);
        assertThat(testBooking.getLicenseContentType()).isEqualTo(UPDATED_LICENSE_CONTENT_TYPE);
        assertThat(testBooking.isLicenseValid()).isEqualTo(UPDATED_LICENSE_VALID);
    }

    @Test
    @Transactional
    public void updateNonExistingBooking() throws Exception {
        int databaseSizeBeforeUpdate = bookingRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookingMockMvc.perform(put("/api/bookings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(booking)))
            .andExpect(status().isBadRequest());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBooking() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);

        int databaseSizeBeforeDelete = bookingRepository.findAll().size();

        // Delete the booking
        restBookingMockMvc.perform(delete("/api/bookings/{id}", booking.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
