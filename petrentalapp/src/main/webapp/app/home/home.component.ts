import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ICar } from 'app/shared/model/car.model';
import { CarService } from 'app/entities/car/car.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { JhiEventManager, JhiParseLinks, JhiDataUtils } from 'ng-jhipster';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;

  cars: ICar[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    protected carService: CarService,
    protected parseLinks: JhiParseLinks,
    protected dataUtils: JhiDataUtils
  ) {
    this.cars = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
  }

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.loadAll();
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  loadAll(): void {
    this.carService
      .query({
        page: this.page,
        size: this.itemsPerPage,
      })
      .subscribe((res: HttpResponse<ICar[]>) => this.paginateCars(res.body, res.headers));
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }
  protected paginateCars(data: ICar[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.cars.push(data[i]);
      }
    }
  }
  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  trackId(index: number, item: ICar): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
}
