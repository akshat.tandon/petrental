import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PetrentalSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [PetrentalSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class PetrentalHomeModule {}
