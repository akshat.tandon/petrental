import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IBooking, Booking } from 'app/shared/model/booking.model';
import { BookingService } from './booking.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { ICar } from 'app/shared/model/car.model';
import { CarService } from 'app/entities/car/car.service';

@Component({
  selector: 'jhi-booking-update',
  templateUrl: './booking-update.component.html',
})
export class BookingUpdateComponent implements OnInit {
  isSaving = false;
  cars: ICar[] = [];

  editForm = this.fb.group({
    id: [],
    bookingtime: [null, [Validators.required]],
    bookingStarttime: [null, [Validators.required]],
    bookingEndtime: [null, [Validators.required]],
    pickupPoint: [null, [Validators.required]],
    dropPoint: [null, [Validators.required]],
    license: [null, [Validators.required]],
    licenseContentType: [],
    licenseValid: [],
    cars: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected bookingService: BookingService,
    protected carService: CarService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ booking }) => {
      if (!booking.id) {
        const today = moment().startOf('day');
        booking.bookingtime = today;
        booking.bookingStarttime = today;
        booking.bookingEndtime = today;
      }

      this.updateForm(booking);

      this.carService.query().subscribe((res: HttpResponse<ICar[]>) => (this.cars = res.body || []));
    });
  }

  updateForm(booking: IBooking): void {
    this.editForm.patchValue({
      id: booking.id,
      bookingtime: booking.bookingtime ? booking.bookingtime.format(DATE_TIME_FORMAT) : null,
      bookingStarttime: booking.bookingStarttime ? booking.bookingStarttime.format(DATE_TIME_FORMAT) : null,
      bookingEndtime: booking.bookingEndtime ? booking.bookingEndtime.format(DATE_TIME_FORMAT) : null,
      pickupPoint: booking.pickupPoint,
      dropPoint: booking.dropPoint,
      license: booking.license,
      licenseContentType: booking.licenseContentType,
      licenseValid: booking.licenseValid,
      cars: booking.cars,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('petrentalApp.error', { message: err.message })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const booking = this.createFromForm();
    if (booking.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingService.update(booking));
    } else {
      this.subscribeToSaveResponse(this.bookingService.create(booking));
    }
  }

  private createFromForm(): IBooking {
    return {
      ...new Booking(),
      id: this.editForm.get(['id'])!.value,
      bookingtime: this.editForm.get(['bookingtime'])!.value
        ? moment(this.editForm.get(['bookingtime'])!.value, DATE_TIME_FORMAT)
        : undefined,
      bookingStarttime: this.editForm.get(['bookingStarttime'])!.value
        ? moment(this.editForm.get(['bookingStarttime'])!.value, DATE_TIME_FORMAT)
        : undefined,
      bookingEndtime: this.editForm.get(['bookingEndtime'])!.value
        ? moment(this.editForm.get(['bookingEndtime'])!.value, DATE_TIME_FORMAT)
        : undefined,
      pickupPoint: this.editForm.get(['pickupPoint'])!.value,
      dropPoint: this.editForm.get(['dropPoint'])!.value,
      licenseContentType: this.editForm.get(['licenseContentType'])!.value,
      license: this.editForm.get(['license'])!.value,
      licenseValid: this.editForm.get(['licenseValid'])!.value,
      cars: this.editForm.get(['cars'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBooking>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICar): any {
    return item.id;
  }

  getSelected(selectedVals: ICar[], option: ICar): ICar {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
