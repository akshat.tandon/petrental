import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'car',
        loadChildren: () => import('./car/car.module').then(m => m.PetrentalCarModule),
      },
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then(m => m.PetrentalBookingModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class PetrentalEntityModule {}
