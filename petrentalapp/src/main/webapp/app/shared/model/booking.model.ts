import { Moment } from 'moment';
import { ICar } from 'app/shared/model/car.model';

export interface IBooking {
  id?: number;
  bookingtime?: Moment;
  bookingStarttime?: Moment;
  bookingEndtime?: Moment;
  pickupPoint?: string;
  dropPoint?: string;
  licenseContentType?: string;
  license?: any;
  licenseValid?: boolean;
  cars?: ICar[];
}

export class Booking implements IBooking {
  constructor(
    public id?: number,
    public bookingtime?: Moment,
    public bookingStarttime?: Moment,
    public bookingEndtime?: Moment,
    public pickupPoint?: string,
    public dropPoint?: string,
    public licenseContentType?: string,
    public license?: any,
    public licenseValid?: boolean,
    public cars?: ICar[]
  ) {
    this.licenseValid = this.licenseValid || false;
  }
}
