import { IBooking } from 'app/shared/model/booking.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface ICar {
  id?: number;
  name?: string;
  model?: string;
  capacity?: number;
  registrationNumber?: string;
  rate?: number;
  status?: Status;
  imageContentType?: string;
  image?: any;
  books?: IBooking[];
}

export class Car implements ICar {
  constructor(
    public id?: number,
    public name?: string,
    public model?: string,
    public capacity?: number,
    public registrationNumber?: string,
    public rate?: number,
    public status?: Status,
    public imageContentType?: string,
    public image?: any,
    public books?: IBooking[]
  ) {}
}
