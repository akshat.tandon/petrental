package com.test.petrental.domain;

import java.io.Serializable;

public class LicenseStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    private String licensename;

    private Boolean licensestatus;

    public String getLicensename() {
        return licensename;
    }

    public void setLicensename(String licensename) {
        this.licensename = licensename;
    }

    public Boolean getLicensestatus() {
        return licensestatus;
    }

    public void setLicensestatus(Boolean licensestatus) {
        this.licensestatus = licensestatus;
    }

    public LicenseStatus(String licensename, Boolean licensestatus) {
        this.licensename = licensename;
        this.licensestatus = licensestatus;
    }

    public LicenseStatus(){}
}
