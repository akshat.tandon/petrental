package com.test.petrental.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Booking.
 */
@Entity
@Table(name = "booking")
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "bookingtime", nullable = false)
    private Instant bookingtime;

    @NotNull
    @Column(name = "booking_starttime", nullable = false)
    private Instant bookingStarttime;

    @NotNull
    @Column(name = "booking_endtime", nullable = false)
    private Instant bookingEndtime;

    @NotNull
    @Column(name = "pickup_point", nullable = false)
    private String pickupPoint;

    @NotNull
    @Column(name = "drop_point", nullable = false)
    private String dropPoint;

    
    @Lob
    @Column(name = "license", nullable = false)
    private byte[] license;

    @Column(name = "license_content_type", nullable = false)
    private String licenseContentType;

    @Column(name = "license_valid")
    private Boolean licenseValid;

    @ManyToMany
    @JoinTable(name = "booking_car",
               joinColumns = @JoinColumn(name = "booking_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "car_id", referencedColumnName = "id"))
    private Set<Car> cars = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getBookingtime() {
        return bookingtime;
    }

    public Booking bookingtime(Instant bookingtime) {
        this.bookingtime = bookingtime;
        return this;
    }

    public void setBookingtime(Instant bookingtime) {
        this.bookingtime = bookingtime;
    }

    public Instant getBookingStarttime() {
        return bookingStarttime;
    }

    public Booking bookingStarttime(Instant bookingStarttime) {
        this.bookingStarttime = bookingStarttime;
        return this;
    }

    public void setBookingStarttime(Instant bookingStarttime) {
        this.bookingStarttime = bookingStarttime;
    }

    public Instant getBookingEndtime() {
        return bookingEndtime;
    }

    public Booking bookingEndtime(Instant bookingEndtime) {
        this.bookingEndtime = bookingEndtime;
        return this;
    }

    public void setBookingEndtime(Instant bookingEndtime) {
        this.bookingEndtime = bookingEndtime;
    }

    public String getPickupPoint() {
        return pickupPoint;
    }

    public Booking pickupPoint(String pickupPoint) {
        this.pickupPoint = pickupPoint;
        return this;
    }

    public void setPickupPoint(String pickupPoint) {
        this.pickupPoint = pickupPoint;
    }

    public String getDropPoint() {
        return dropPoint;
    }

    public Booking dropPoint(String dropPoint) {
        this.dropPoint = dropPoint;
        return this;
    }

    public void setDropPoint(String dropPoint) {
        this.dropPoint = dropPoint;
    }

    public byte[] getLicense() {
        return license;
    }

    public Booking license(byte[] license) {
        this.license = license;
        return this;
    }

    public void setLicense(byte[] license) {
        this.license = license;
    }

    public String getLicenseContentType() {
        return licenseContentType;
    }

    public Booking licenseContentType(String licenseContentType) {
        this.licenseContentType = licenseContentType;
        return this;
    }

    public void setLicenseContentType(String licenseContentType) {
        this.licenseContentType = licenseContentType;
    }

    public Boolean isLicenseValid() {
        return licenseValid;
    }

    public Booking licenseValid(Boolean licenseValid) {
        this.licenseValid = licenseValid;
        return this;
    }

    public void setLicenseValid(Boolean licenseValid) {
        this.licenseValid = licenseValid;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public Booking cars(Set<Car> cars) {
        this.cars = cars;
        return this;
    }

    public Booking addCar(Car car) {
        this.cars.add(car);
        car.getBooks().add(this);
        return this;
    }

    public Booking removeCar(Car car) {
        this.cars.remove(car);
        car.getBooks().remove(this);
        return this;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Booking)) {
            return false;
        }
        return id != null && id.equals(((Booking) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Booking{" +
            "id=" + getId() +
            ", bookingtime='" + getBookingtime() + "'" +
            ", bookingStarttime='" + getBookingStarttime() + "'" +
            ", bookingEndtime='" + getBookingEndtime() + "'" +
            ", pickupPoint='" + getPickupPoint() + "'" +
            ", dropPoint='" + getDropPoint() + "'" +
            ", license='" + getLicense() + "'" +
            ", licenseContentType='" + getLicenseContentType() + "'" +
            ", licenseValid='" + isLicenseValid() + "'" +
            "}";
    }
}
