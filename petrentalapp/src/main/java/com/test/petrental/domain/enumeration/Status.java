package com.test.petrental.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    BOOKED, FREE
}
