package com.test.petrental.domain;

import java.io.Serializable;

public class License implements Serializable {

    private static final long serialVersionUID = 1L;

    private byte[] license;

    private String licenseContentType;

    private String licensename;

    public byte[] getLicense() {
        return license;
    }

    public void setLicense(byte[] license) {
        this.license = license;
    }

    public String getLicenseContentType() {
        return licenseContentType;
    }

    public void setLicenseContentType(String licenseContentType) {
        this.licenseContentType = licenseContentType;
    }

    public String getLicensename() {
        return licensename;
    }

    public void setLicensename(String licensename) {
        this.licensename = licensename;
    }

    public License(){}

    public License(byte[] license, String licenseContentType ,String licensename ){
        this.license = license;
        this.licenseContentType = licenseContentType;
        this.licensename = licensename;
    }
}


