package com.test.petrental.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class LicenseException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public LicenseException() {
        super(ErrorConstants.INVALID_LICENSE, "Invalid License", Status.BAD_REQUEST);
    }
}
