/**
 * View Models used by Spring MVC REST controllers.
 */
package com.test.petrental.web.rest.vm;
